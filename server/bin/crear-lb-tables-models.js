/**
 * Descubre y crea los modelos del proyecto en base a las tablas de la base de dato
 * que esten definidas a ser creadas en este documento.
 */

'use strict';

const loopback = require('loopback');
const promisify = require('util').promisify;
const fs = require('fs');
const writeFile = promisify(fs.writeFile);
const readFile = promisify(fs.readFile);
const mkdirp = promisify(require('mkdirp'));
const DATASOURCE_NAME = 'lotta'; // Este es el nombre del datasource
const dataSourceConfig = require('../datasources.json');
const mysql = new loopback.DataSource(dataSourceConfig[DATASOURCE_NAME]);
discover().then(
    success => process.exit(),
    error => { console.error('UNHANDLED ERROR:\n', error); process.exit(1); },
);
async function discover() {
    // Esta es la opcion que le permite a dataSource.discoverSchema() buscar las rela    ciones que hay entre las tablas.
    const options = { relations: true };

    // descubre los modelos con su relaciones
    const jugadaSchemas = await mysql.discoverSchemas('jugada', options);
    const loteriaSchemas = await mysql.discoverSchemas('loteria', options);
    const numeroSchemas = await mysql.discoverSchemas('numero', options);
    const numeros_limitadoSchemas = await mysql.discoverSchemas('numero_limitado', options);
    const sorteoSchemas = await mysql.discoverSchemas('sorteo', options);
    const sucursalesSchemas = await mysql.discoverSchemas('sucursal', options);
    const configuracionesSchemas = await mysql.discoverSchemas('configuraciones', options);

    await mkdirp('common/models');
    const database = "lotta";
    await writeFile(
        'common/models/jugada.json',
        JSON.stringify(jugadaSchemas[`${database}.jugada`], null, 2)
    );
    await writeFile(
        'common/models/loteria.json',
        JSON.stringify(loteriaSchemas[`${database}.loteria`], null, 2)
    );
    await writeFile(
        'common/models/numero.json',
        JSON.stringify(numeroSchemas[`${database}.numero`], null, 2)
    );
    await writeFile(
        'common/models/numeros_limitados.json',
        JSON.stringify(numeros_limitadoSchemas[`${database}.numero_limitado`], null, 2)
    );
    await writeFile(
        'common/models/sorteo.json',
        JSON.stringify(sorteoSchemas[`${database}.sorteo`], null, 2)
    );
    await writeFile(
        'common/models/sucursales.json',
        JSON.stringify(sucursalesSchemas[`${database}.sucursal`], null, 2)
    );
    await writeFile(
        'common/models/ventas.json',
        JSON.stringify(configuracionesSchemas[`${database}.configuraciones`], null, 2)
    );

    // Exponer los modelos via REST API
    const configJson = await readFile('server/model-config.json', 'utf-8');
    console.log('MODEL CONFIG', configJson);
    const config = JSON.parse(configJson);

    // El nombre que se le ponga a la variable config.Name será el nombre que se usará para la API.
    config.Jugada = { dataSource: DATASOURCE_NAME, public: true };
    config.Loteria = { dataSource: DATASOURCE_NAME, public: true };
    config.Numero = { dataSource: DATASOURCE_NAME, public: true };
    config.NumeroLimitado = { dataSource: DATASOURCE_NAME, public: true };
    config.Sorteo = { dataSource: DATASOURCE_NAME, public: true };
    config.Sucursal = { dataSource: DATASOURCE_NAME, public: true };
    config.Configuraciones = { dataSource: DATASOURCE_NAME, public: true };

    await writeFile(
        'server/model-config.json',
        JSON.stringify(config, null, 2)
    );    
}